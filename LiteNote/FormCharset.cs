using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteNote
{
    partial class FormCharset : Form
    {
        public FormCharset()
        {
            InitializeComponent();
        }

        private void FormCharset_Load(object sender, EventArgs e)
        {
            cmbCharset.SelectedIndex = 0;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        // 设置要显示的文件路径
        public void setPath(string path)
        {
            txtPath.Text = path;
        }

        // 获取设置的字符集
        public string getCharset()
        {
            //return cmbCharset.SelectedText;

            if (rdbFix.Checked) {
                return cmbCharset.SelectedItem.ToString();
            } else {
                return txtCharset.Text;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
