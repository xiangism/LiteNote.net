using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteNote
{
    partial class FormEditApp : Form
    {
        public FormEditApp()
        {
            InitializeComponent();

            Load += FormEditApp_Load;
        }

        LiteNoteSetting settings = new LiteNoteSetting();

        private void FormEditApp_Load(object sender, EventArgs e)
        {
            txtPath.Text = settings.EditApp;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string path = txtPath.Text;
            if (path.ToLower().EndsWith(".exe") && File.Exists(path)) {
                settings.EditApp = path;
                settings.Save();
            }
            this.Close();
            this.DialogResult = DialogResult.OK;
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                txtPath.Text = openFileDialog1.FileName;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
