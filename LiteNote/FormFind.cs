using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteNote
{
    partial class FormFind : Form
    {
        public FormFind(NoteForm frm)
        {
            InitializeComponent();
            noteForm = frm;
        }

        NoteForm noteForm;
        SearchNote search = new SearchNote();

        int img_index_fold = 0;
        int img_index_note = 1;

        void startSearch()
        {
            if (txtSearch.Text.Length == 0)
                return;

            List<string> ws = noteForm.GetWorksets();

            search.setSearchPath(ws);
            search.IsSearchContent = chbContent.Checked;
            search.IsSearchNoteName = chbNote.Checked;
            search.IsSearchFoldName = chbFold.Checked;
            search.IsSearchAa = chbAa.Checked;

            Dictionary<string, string> rs = search.search(txtSearch.Text);

            lsvResult.Items.Clear();

            foreach (var one in rs) {
                var path = one.Key;

                string[] fs = { Path.GetFileNameWithoutExtension(path), one.Value };

                ListViewItem item = new ListViewItem(fs);
                item.Tag = path;

                if (File.Exists(path)) {
                    item.ImageIndex = img_index_note;
                } else if (Directory.Exists(path)) {
                    item.ImageIndex = img_index_fold;
                }

                lsvResult.Items.Add(item);
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            startSearch();
        }

        private void FormFind_Load(object sender, EventArgs e)
        {
            imageList1.ColorDepth = ColorDepth.Depth32Bit;
            imageList1.ImageSize = new Size(32, 32);

            imageList1.Images.Clear();
            imageList1.Images.Add(Properties.Resources.fold);
            imageList1.Images.Add(Properties.Resources.note);
        }

        private void chbContent_CheckedChanged(object sender, EventArgs e)
        {
            search.IsSearchContent = chbContent.Checked;
        }

        private void chbNote_CheckedChanged(object sender, EventArgs e)
        {
            search.IsSearchNoteName = chbNote.Checked;
        }

        private void chbFold_CheckedChanged(object sender, EventArgs e)
        {
            search.IsSearchFoldName = chbFold.Checked;
        }

        private void chbAa_CheckedChanged(object sender, EventArgs e)
        {
            search.IsSearchAa = chbAa.Checked;
        }

        private void lsvResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (var item in lsvResult.SelectedIndices) {
                var path = lsvResult.Items[(int)item].Tag.ToString();
                //noteForm.FocusTree();
                noteForm.NavigationTree(path);
                break;
            }
        }

        private void FormFind_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void lsvResult_Resize(object sender, EventArgs e)
        {
            ColumnHeader h1 = lsvResult.Columns[0];
            ColumnHeader h2 = lsvResult.Columns[1];

            h2.Width = lsvResult.Width - h1.Width-10;
        }

        private void lsvResult_MouseUp(object sender, MouseEventArgs e)
        {
            foreach (var item in lsvResult.SelectedIndices) {
                var path = lsvResult.Items[(int)item].Tag.ToString();
                noteForm.FocusTree();
                noteForm.NavigationTree(path);
                break;
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                startSearch();
            }
        }

        private void FormFind_Enter(object sender, EventArgs e)
        {
            Console.WriteLine("enter");
            txtSearch.Focus();
        }

        private void FormFind_Activated(object sender, EventArgs e)
        {
            Console.WriteLine("activer");
            txtSearch.Focus();
            txtSearch.SelectAll();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
