using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Xml;
using Microsoft.VisualBasic.FileIO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace LiteNote
{
    partial class NoteForm : Form
    {
        [DllImport("Markdown.dll")]
        static extern void SynatxMarkdown(string text, string file_dir, int font_size, StringBuilder out_str);

        // 用于在多线程中进行markdown到html的转化
        string txtFilename = "";
        object lockThread = new object();
        SynchronizationContext syncContext = null;
        AutoResetEvent mkEvent = new AutoResetEvent(false);

        // 存储设置属性
        LiteNoteSetting settings = new LiteNoteSetting();

        // 用于接受SynatxMarkdown()的输出字符串
        StringBuilder out_str = new StringBuilder(200*1024*1024);

        // 刷新网页时网页滚动的位置，用于加载完后跳转到原来的浏览位置
        int refreshScrollTop = 0;

        string rootPath = Path.GetDirectoryName(Application.ExecutablePath);

        public NoteForm()
        {
            //File.AppendAllText("size.txt", "NoteFormCreaet\n");

            InitializeComponent();
            findForm = new FormFind(this);
        }

        // 提供一个全路径，然后定位到树导航节点中
        public void NavigationTree(string path)
        {
            Console.WriteLine("Navigation. {0}", path);

            for (int i = 0; i < settings.WorksetPaths.Count; ++i) {

                string s = settings.WorksetPaths[i];

                if (path.StartsWith(s)) {

                    TreeNode n = treeView1.Nodes[i];

                    n.Expand();
                    //RefreshNode(n); // 展开后会自动调用RefreshNode方法

                    treeView1.SelectedNode = n;

                    NavigationNode(n, path.Substring(s.Length));
                }
            }
        }

        // 将焦点定位到树控件
        public void FocusTree()
        {
            this.Focus();
            treeView1.Focus();
        }

        // 获取所有工作集目录
        public List<string> GetWorksets()
        {
            List<string> workset = new List<string>();

            foreach(string s in settings.WorksetPaths) {
                workset.Add(s);
            }

            return workset;
        }

        // 在node节点中定位path\\前面的路径, 定位到后截取path，再在node的子节点中递归定位
        void NavigationNode(TreeNode node, string path)
        {
            if (path.StartsWith("\\"))
                path = path.Substring(1);

            if (path.Length == 0)
                return;

            string now = path;
            string last = "";
            int index = path.IndexOf('\\');
            if (index != -1) {
                now = path.Substring(0, index);
                last = path.Substring(index + 1);
            }
            if (last.Length == 0 && (now.EndsWith(".txt") || now.EndsWith(".md"))) {
                now = now.Substring(0, now.Length - 4);
            }

            foreach (TreeNode n in node.Nodes) {
                if (n == null)
                    continue;

                if (n.Text == now) {

                    n.Expand();
                    //RefreshNode(n); // 展开后会自动调用RefreshNode方法

                    treeView1.SelectedNode = n;

                    if (last.Length != 0) {
                        NavigationNode(n, last);
                    }
                }
            }
        }

        private void NoteForm_Load(object sender, EventArgs e)
        {
            imageList1.ColorDepth = ColorDepth.Depth32Bit;
            imageList1.ImageSize = new Size(32, 32);

            imageList1.Images.Clear();
            imageList1.Images.Add(Properties.Resources.workset);
            imageList1.Images.Add(Properties.Resources.fold);
            imageList1.Images.Add(Properties.Resources.note);

            syncContext = SynchronizationContext.Current;
            settings.Reload();

            ReadSetting();
            ReadCharsets();

            ReLoadTree();

            itemIsCollapseBrother.Checked = settings.IsCollapseBrother;
            itemOneClickMode.Checked = settings.OneClickMode;
            //itemIsCollapseChilds.Checked = settings.IsCollapseChild;
            setOneClickMode(settings.OneClickMode);

            webBrowser1.DocumentText = "";

            Thread thread = new Thread(new ThreadStart(MarkdownThread));
            thread.IsBackground = true;
            thread.Start();

            //findForm.Show();
            formIniting = false;
        }

        void setOneClickMode(bool isOneClick)
        {
            int globalClass = 0x4008;
            if (isOneClick) {
                // 移除treeViewNote的双击事件
                globalClass = 0x4000;
            }

            const int GCL_STYLE = -26;
            Utility.SetClassLong(treeView1.Handle, GCL_STYLE, globalClass);
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //saveSizeAndLocation();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            settings.LeftWidth = splitContainer1.SplitterDistance;
            settings.Save();
        }

        private void NoteForm_Resize(object sender, EventArgs e)
        {
            saveSizeAndLocation();
        }

        private void NoteForm_LocationChanged(object sender, EventArgs e)
        {
            saveSizeAndLocation();
        }

        private void saveSizeAndLocation()
        {
            // 为了防止在窗体初始化时，将初始化大小又写到setting中去，所以得等初始化完毕后才能写setting
            if (formIniting)
                return;

            if (Width > 0 && Height > 0 && Left > 0 && Top > 0) {

                settings.Left = Left;
                settings.Top = Top;
                settings.Width = Width;
                settings.Height = Height;
                settings.Save();

            }
        }
        bool formIniting = true;

        // 读取配置
        void ReadSetting()
        {
            if (settings.LeftWidth != 0) {
                splitContainer1.SplitterDistance = settings.LeftWidth;
            }

            if (settings.Width > 0 && settings.Height > 0) {

                if (settings.Width < 200 || settings.Height < 200) {
                    this.Width = 1000;
                    this.Height = 700;
                } else {
                    this.Width = settings.Width;
                    this.Height = settings.Height;
                }
            }

            int left = settings.Left;
            int top = settings.Top;
            if (left > 0 && left < Screen.PrimaryScreen.Bounds.Width &&
                top > 0 && top < Screen.PrimaryScreen.Bounds.Height) {
                this.Left = settings.Left;
                this.Top = settings.Top;
            }
        }

        string getCharsetFilename()
        {
            return "charset.dat";
        }

        // filename => char name
        Dictionary<string, string> charsets = new Dictionary<string, string>();

        void ReadCharsets()
        {
            try {
                using (FileStream reader = new FileStream(getCharsetFilename(), FileMode.Open)) {
                    IFormatter formatter = new BinaryFormatter();
                    charsets = formatter.Deserialize(reader) as Dictionary<string, string>;
                }

            } catch (System.IO.FileNotFoundException) { }
        }

        void AddCharset(string path, string charset)
        {
            if (charsets.ContainsKey(path))
                charsets[path] = charset;
            else
                charsets.Add(path, charset);

            using (FileStream writer = new FileStream(getCharsetFilename(), FileMode.Create)) {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(writer, charsets);
            }
        }


        #region Thread

        void ThreadShowWeb(object obj)
        {
            if (obj is string) {
                string str = obj as string;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(str);

                XmlNode root = doc.FirstChild;
                XmlNode html = root.FirstChild;
                XmlNode anchor = root.LastChild;

                AnchorNode node = new AnchorNode();
                ScanAnchorNode(anchor, node);
                RefershAnchorTree(node);

                if (webBrowser1.Document != null) {
                    refreshScrollTop = webBrowser1.Document.Body.ScrollTop;
                }
                webBrowser1.DocumentText = html.InnerText;
            }
        }

        void RefershAnchorTree(AnchorNode node)
        {
            treeView2.Nodes.Clear();

            for (int i = 0; i < node.Children.Count; ++i) {
                TreeNode treeOne = new TreeNode();

                RefreshAnchorOne(treeOne, node.Children[i]);
                treeView2.Nodes.Add(treeOne);
            }

            treeView2.ExpandAll();
        }

        void RefreshAnchorOne(TreeNode treeNode, AnchorNode dataNode)
        {
            treeNode.Text = dataNode.Name;
            treeNode.Tag = dataNode.AnchorID;

            for (int i = 0; i < dataNode.Children.Count; ++i) {
                TreeNode c = new TreeNode();
                RefreshAnchorOne(c, dataNode.Children[i]);
                treeNode.Nodes.Add(c);
            }
        }

        void MarkdownThread()
        {
            while (true) {

                string file = "";
                lock (lockThread) {
                    file = txtFilename;
                    txtFilename = "";
                }

                if (file.Length != 0) {

                    /*FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
                    StreamReader sr = new StreamReader(fs, Encoding.UTF8);
                    string str = "";
                    while (!sr.EndOfStream) {
                        str += sr.ReadLine() + "\n";
                    }
                    sr.Close();
                    fs.Close();*/

                    string str = "";
                    try {

                        Encoding en = Encoding.UTF8;

                        if (charsets.ContainsKey(file)) {
                            try {
                                en = Encoding.GetEncoding(charsets[file]);
                            } catch (System.ArgumentException) { }

                        }

                        str = File.ReadAllText(file, en);
                    } catch (IOException e) {

                    }

                    if (str.Length > 0) {

                        SynatxMarkdown(str, Path.GetDirectoryName(file), settings.FontSize, out_str);
                        string s = out_str.ToString();
                        syncContext.Post(ThreadShowWeb, s);
                    }

                } else {
                    // 等待数据
                    mkEvent.WaitOne();
                }
            }

        }
        #endregion

        #region TreeViewControl

        static int workset_img_index = 0;
        static int fold_img_index = 1;
        static int note_img_index = 2;

        void ReLoadTree()
        {
            if (settings.WorksetNames == null) {
                settings.WorksetNames = new System.Collections.Specialized.StringCollection();
                settings.WorksetPaths = new System.Collections.Specialized.StringCollection();
            }

            if (settings.WorksetNames.Count == 0) {
                string s = Application.ExecutablePath;
                int index = s.LastIndexOf('\\');
                string path = s.Substring(0, index);

                settings.WorksetNames.Add("当前路径");
                settings.WorksetPaths.Add(path);
            }

            treeView1.Nodes.Clear();
            treeView1.SelectedNode = null;
            AddRootNodes();

            NavigationTree(settings.NowPath);
        }

        //调用外部程序对笔记进行编辑
        void EditNode()
        {
            if (treeView1.SelectedNode != null && treeView1.SelectedNode.Tag is FileInfo) {
                FileInfo info = treeView1.SelectedNode.Tag as FileInfo;

                if (Utility.IsTextFile(info.FullName)) {
                    Utility.EditInApp(settings.EditApp, info.FullName);
                    fileSystemWatcher1.Path = Path.GetDirectoryName(info.FullName);
                }
            }
        }

        void AddNote()
        {
            if (treeView1.SelectedNode == null) {
                return;
            }

            if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                DirectoryInfo info = treeView1.SelectedNode.Tag as DirectoryInfo;
                AddNote(info.FullName);

            } else if (treeView1.SelectedNode.Tag is FileInfo) {
                FileInfo info = treeView1.SelectedNode.Tag as FileInfo;
                var p = Path.GetDirectoryName(info.FullName);

                AddNote(p);
            }
        }

        // @path 文件夹路径
        void AddNote(string path)
        {
            path = Utility.GetNoExistNote(path);

            StreamWriter sr = File.CreateText(path);
            sr.Close();

            if (treeView1.SelectedNode != null) {

                // 如果点击的是文本节点，则应该刷新其父节点
                if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                    RefreshNode(treeView1.SelectedNode);
                } else {
                    RefreshNode(treeView1.SelectedNode.Parent);
                }


                foreach (TreeNode n in treeView1.SelectedNode.Nodes) {
                    if (n.Tag is FileInfo) {
                        FileInfo f = n.Tag as FileInfo;
                        if (f.FullName == path) {
                            treeView1.SelectedNode = n;
                            break;
                        }
                    }
                }
            } else {
                // 如果selectNode为空则是在顶层添加节点
                ReLoadTree();

                foreach (TreeNode n in treeView1.Nodes) {
                    if (n.Tag is FileInfo) {
                        FileInfo f = n.Tag as FileInfo;
                        if (f.FullName == path) {
                            treeView1.SelectedNode = n;
                            break;
                        }
                    }
                }
            }

            RenameSelectNote();
        }

        void AddFold()
        {
            if (treeView1.SelectedNode == null) {
                return;
            }
            if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                DirectoryInfo info = treeView1.SelectedNode.Tag as DirectoryInfo;

                AddFold(info.FullName);

            } else if (treeView1.SelectedNode.Tag is FileInfo) {
                FileInfo info = treeView1.SelectedNode.Tag as FileInfo;
                var p = Path.GetDirectoryName(info.FullName);

                AddFold(p);
            }
        }

        // 在path文件夹路径下创建文件夹
        void AddFold(string path)
        {
            path = Utility.GetNoExistFold(path);

            Directory.CreateDirectory(path);

            if (treeView1.SelectedNode != null) {

                if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                    RefreshNode(treeView1.SelectedNode);
                } else {
                    RefreshNode(treeView1.SelectedNode.Parent);
                }

                foreach (TreeNode n in treeView1.SelectedNode.Nodes) {
                    if (n.Tag is DirectoryInfo) {
                        DirectoryInfo d = n.Tag as DirectoryInfo;
                        if (d.FullName == path) {
                            treeView1.SelectedNode = n;
                            break;
                        }
                    }
                }
            } else {
                // 如果selectNode为空则是在顶层添加节点
                ReLoadTree();

                foreach (TreeNode n in treeView1.Nodes) {
                    if (n.Tag is DirectoryInfo) {
                        DirectoryInfo d = n.Tag as DirectoryInfo;
                        if (d.FullName == path) {
                            treeView1.SelectedNode = n;
                            break;
                        }
                    }
                }
            }

            RenameSelectNote();
        }

        bool ShowConfirmFrom(string title)
        {
            return MessageBox.Show(title, "LiteNote", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes;
        }

        void DeleteNode()
        {
            TreeNode now_node = treeView1.SelectedNode;

            if (now_node == null) {
                return;
            }

            if (now_node.Parent == null) {
                // 是根节点

                if (ShowConfirmFrom("确定移除此工作集")) {

                    settings.WorksetNames.RemoveAt(now_node.Index);
                    settings.WorksetPaths.RemoveAt(now_node.Index);
                    ReLoadTree();
                }
                return;
            }

            if (!ShowConfirmFrom("是否删除")) {
                return;
            }

            if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                DirectoryInfo info = treeView1.SelectedNode.Tag as DirectoryInfo;
                Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(info.FullName, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                // TODO:实现成通用文案

                RefreshNode(treeView1.SelectedNode.Parent);

            } else if (treeView1.SelectedNode.Tag is FileInfo) {
                FileInfo info = treeView1.SelectedNode.Tag as FileInfo;
                Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(info.FullName, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                // TODO:实现成通用文案

                RefreshNode(treeView1.SelectedNode.Parent);
            }

        }

        void AddRootNodes()
        {
            for (int i = 0; i < settings.WorksetNames.Count; ++i) {
                AddRootNode(settings.WorksetNames[i], settings.WorksetPaths[i]);
            }
        }

        void AddRootNode(string name, string path)
        {
            TreeNode root_node = new TreeNode(name);

            DirectoryInfo di = new DirectoryInfo(path);
            root_node.Tag = di;
            root_node.ImageIndex = workset_img_index;
            root_node.SelectedImageIndex = workset_img_index;

            treeView1.Nodes.Add(root_node);

            // TODO:这里添加工作集的概念
            AddNodes(root_node.Nodes, di);

            foreach (TreeNode node in root_node.Nodes) {
                if (node.Tag is DirectoryInfo) {
                    AddNodes(node.Nodes, node.Tag as DirectoryInfo);
                }
            }
        }

        void AddNodes(TreeNodeCollection nodes, DirectoryInfo info)
        {
            if (!Utility.IsNormalFold(info.FullName)) {
                return;
            }

            DirectoryInfo[] ds = new DirectoryInfo[] { };
            FileInfo[] fs = new FileInfo[] { };
            try {
                ds = info.GetDirectories();
                fs = info.GetFiles();
            } catch (System.UnauthorizedAccessException) { }

            foreach (DirectoryInfo d in ds) {
                string name = d.Name;

                if (!Utility.IsNormalFold(d.FullName))
                    continue;

                TreeNode dirnode = new TreeNode(name);
                dirnode.Tag = d;

                dirnode.ImageIndex = fold_img_index;
                dirnode.SelectedImageIndex = fold_img_index;

                nodes.Add(dirnode);
            }

            foreach (FileInfo f in fs) {

                TreeNode filenode = new TreeNode(Path.GetFileNameWithoutExtension(f.Name));
                filenode.Tag = f;
                bool is_add = false;

                if (Utility.IsTextFile(f.FullName)) {
                    filenode.ImageIndex = note_img_index;
                    filenode.SelectedImageIndex = note_img_index;
                    is_add = true;

                } else {
                }

                //如果是隐藏文件则不加入
                if ((f.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden) {
                    is_add = false;
                }

                if (is_add) {
                    nodes.Add(filenode);
                }
            }

        }//end function

        List<string> getNodeSet(TreeNode node)
        {
            List<string> ls = new List<string>();
            foreach(TreeNode one in node.Nodes) {

                if (one.Tag is DirectoryInfo) {
                    ls.Add("d:"+ (one.Tag as DirectoryInfo).Name);

                } else if (one.Tag is FileInfo) {

                    ls.Add("f:"+ (one.Tag as FileInfo).Name);
                }
            }

            return ls;
        }

        List<string> getFoldSet(DirectoryInfo dir)
        {
            List<string> ls = new List<string>();

            foreach (var one in dir.GetFiles()) {
                if (!Utility.IsTextFile(one.FullName)) {
                    continue;
                }

                ls.Add("f:" + one.Name);
            }

            foreach (var one in dir.GetDirectories()) {
                if (!Utility.IsNormalFold(one.FullName)) {
                    continue;
                }
                ls.Add("d:" + one.Name);
            }

            return ls;
        }

        void RefreshNode(TreeNode node, bool isFocus = false)
        {
            if (node == null) {
                ReLoadTree();
                return;
            }
            if (isFocus) {
                RealRefresh(node);
                return;
            }

            if (node.Tag is DirectoryInfo) {

                DirectoryInfo dir = node.Tag as DirectoryInfo;

                List<string> s1 = getNodeSet(node);
                List<string> s2 = getFoldSet(dir);

                //if (s1.Except(s2).Count() != 0 || s2.Except(s1).Count() != 0)
                {
                    RealRefresh(node);
                }
            }
        }

        void RealRefresh(TreeNode node)
        {
            Console.WriteLine("Refresh Node");
            //Dictionary<string, string> dic = new Dictionary<string, string>();

            // TODO:因为这里的清除机制，所以树控件会闪烁，得想个比较好的办法
            // 如果原来的节点名没有改，则不需要更新

            node.Nodes.Clear();

            DirectoryInfo dir = node.Tag as DirectoryInfo;
            AddNodes(node.Nodes, dir);

            foreach (TreeNode n in node.Nodes) {
                if (n.Tag is DirectoryInfo) {
                    AddNodes(n.Nodes, n.Tag as DirectoryInfo);
                }
            }

            node.Expand();
        }

        void RenameSelectNote()
        {
            if (treeView1.SelectedNode != null) {
                treeView1.LabelEdit = true;
                treeView1.SelectedNode.BeginEdit();
            }
        }

        void RefreshShowNote()
        {
            if (treeView1.SelectedNode != null) {

                if (treeView1.SelectedNode.Tag is DirectoryInfo) {
                    RefreshNode(treeView1.SelectedNode);

                } else if (treeView1.SelectedNode.Tag is FileInfo) {
                    FileInfo f = treeView1.SelectedNode.Tag as FileInfo;
                    ShowText(f.FullName);
                }
            }
        }

        // 现在现在显示的笔记名
        string nowNotePath = "";
        TreeNode nowNoteNode = null;

        void ShowText(string filename)
        {
            int s = Utility.GetFileStyle(filename);

            nowNotePath = filename;
            nowNoteNode = treeView1.SelectedNode;

            if (s == 0) {
                // 判断如果文件为空，则直接显示空白页面
                webBrowser1.DocumentText = "空文件";

            } else if (s == 1) {
                webBrowser1.DocumentText = "已用VimCrypt加密,请使用vim打开";

            } else if (s == 2) {
                lock (lockThread) {
                    txtFilename = filename;
                }
                mkEvent.Set();
            }
        }

        // 从xml结构构建node节点
        void ScanAnchorNode(XmlNode data, AnchorNode node)
        {
            if (data.ChildNodes.Count >= 3) {
                string level = data.ChildNodes[0].FirstChild.InnerText;
                if (level != null && level.Length > 0) {
                    node.Level = Convert.ToInt32(level);
                }

                node.AnchorID = data.ChildNodes[1].FirstChild?.InnerText;
                node.Name = data.ChildNodes[2].FirstChild?.InnerText;
            }

            if (data.ChildNodes.Count >= 4) {

                for (int i = 0; i < data.ChildNodes.Count - 3; ++i) {
                    XmlNode one = data.ChildNodes[i + 3];

                    AnchorNode o2 = new AnchorNode();
                    ScanAnchorNode(one, o2);

                    node.Children.Add(o2);
                }
            }
        }

        #endregion

        #region treeView

        void ToggleNode(TreeNode node)
        {
            if (node != null) {
                if (node.IsExpanded) {
                    node.Collapse();
                } else {
                    node.Expand();
                }
            }
        }

        private void treeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            treeView1.LabelEdit = false;
            if (e.Label == null) {
                return;
            }

            if (e.Node.Parent == null) {
                // 如果是父节点则是工作集节点
                settings.WorksetNames[e.Node.Index] = e.Label;
                return;
            }

            if (e.Node.Tag is DirectoryInfo) {
                DirectoryInfo info = e.Node.Tag as DirectoryInfo;
                string newPath = Path.Combine(Path.GetDirectoryName(info.FullName), e.Label);

                try {
                    Directory.Move(info.FullName, newPath);
                    DirectoryInfo new_info = new DirectoryInfo(newPath);
                    e.Node.Tag = new_info;

                    RefreshNode(e.Node);

                } catch {
                    RefreshNode(e.Node.Parent, true);
                }

            } else if (e.Node.Tag is FileInfo) {
                FileInfo info = e.Node.Tag as FileInfo;
                string newPath = Path.Combine(Path.GetDirectoryName(info.FullName), e.Label + ".md");

                nowNotePath = newPath;

                try {
                    Directory.Move(info.FullName, newPath);
                    FileInfo new_info = new FileInfo(newPath);
                    e.Node.Tag = new_info;
                } catch {
                    RefreshNode(e.Node.Parent, true);
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = e.Node;
            if (node == null)
                return;

            if (node.Tag is FileInfo) {
                FileInfo f = node.Tag as FileInfo;

                if (Utility.IsTextFile(f.FullName)) {
                    ShowText(f.FullName);
                }
                statusLabelNode.Text = f.FullName;

                settings.NowPath = f.FullName;

            } else if (node.Tag is DirectoryInfo) {
                DirectoryInfo d = node.Tag as DirectoryInfo;
                d.Refresh();

                if (d.Exists) {
                    try {

                        string t = string.Format("\"{0}\" 中有{1}个目录,{2}个笔记", node.FullPath, d.GetDirectories().GetLength(0).ToString(), d.GetFiles().GetLength(0).ToString());
                        statusLabelFold.Text = t;
                    } catch (Exception) { }

                } else {
                    statusLabelFold.Text = string.Format("\"{0}\" 目录不存在", node.FullPath);
                }
                settings.NowPath = d.FullName;
            }
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            treeView1.SuspendLayout();
            //treeView1.Scrollable = false;

            RefreshNode(e.Node);

            if (settings.IsCollapseBrother) {

                // 收起兄弟节点
                if (e.Node.Tag is DirectoryInfo) {

                    // 这里只所有只在第一次展开节点时添加子节点，就是因为有IsAddChildNode()判断
                    // 这个函数查找e.Node的所有子节点，如果这些子节点所拥有的子节点的个数不为0，那么就得添加
                    if (TreeNodeManager.IsAddChildNode(e.Node)) {
                        foreach (TreeNode n in e.Node.Nodes) {
                            if (n.Tag is DirectoryInfo) {
                                AddNodes(n.Nodes, n.Tag as DirectoryInfo);
                            }
                        }
                    }

                    if (e.Node.Parent != null) {
                        foreach (TreeNode n in e.Node.Parent.Nodes) {
                            if (n == e.Node)
                                continue;
                            n.Collapse();
                        }
                    } else {
                        foreach (TreeNode n in treeView1.Nodes) {
                            if (n == e.Node)
                                continue;
                            n.Collapse();
                        }
                    }

                    //e.Node.ImageIndex = TreeNodeManager.dir_open_img;
                    //e.Node.SelectedImageIndex = TreeNodeManager.dir_open_img;
                }
            }
            //treeView1.Scrollable = true;
            treeView1.ResumeLayout();
        }

        private void treeView1_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            // 因为会展开时会刷新当前节点，所有如果不另外存储子节点的打开情况的话，怎么都会将子节点收起
            /*
            if (settings.IsCollapseChild) {

                // 将子目录将收起
                if (e.Node.Tag is DirectoryInfo) {
                    //e.Node.ImageIndex = TreeNodeManager.dir_img;
                    //e.Node.SelectedImageIndex = TreeNodeManager.dir_img;

                    foreach (TreeNode n in e.Node.Nodes) {
                        if (e.Node.Tag is DirectoryInfo) {
                            n.Collapse(false);
                        }
                    }
                }
            }
             */
        }

        private void treeView1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) {
                //e.KeyCode == Keys.Delete
                DeleteNode();
            }
        }

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
                ToggleNode(treeView1.SelectedNode);

            if (e.Control && e.KeyCode ==  Keys.F) {
                findForm.Show();
            }
        }

        int preDown = 0;
        TreeNode preNode = null;

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) {

                Point p = Control.MousePosition;
                p = treeView1.PointToClient(p);

                TreeNode node = treeView1.GetNodeAt(p);

                {
                    int now = Environment.TickCount;
                    int diff = now - preDown;
                    preDown = now;

                    if (node == preNode && diff < Utility.GetDoubleClickTime()) {
                        Console.WriteLine("double click");

                        EditNode();
                    }
                }

                preNode = node;
                treeView1.SelectedNode = node;

                if (node != null) {

                    if (node.Parent == null) {
                        // 为根节点
                        treeView1.ContextMenuStrip = contextMenuWorkset;

                    } else {
                        if (node.Tag is FileInfo) {
                            // 右击笔记节点
                            treeView1.ContextMenuStrip = contextMenuNode;

                        } else if (node.Tag is DirectoryInfo) {
                            // 右击目录节点
                            treeView1.ContextMenuStrip = contextMenuFold;
                        }
                    }

                } else {
                    // 右击空白处
                    treeView1.ContextMenuStrip = contextMenuBlack;
                }
            }

        }

        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (settings.OneClickMode) {

                if (e.Button == MouseButtons.Left) {

                    Point p = Control.MousePosition;
                    p = treeView1.PointToClient(p);
                    TreeNode node = treeView1.GetNodeAt(p);

                    if (node == null)
                        return;

                    int diff = node.Bounds.X - p.X;

                    if (diff < imageList1.ImageSize.Width)
                        ToggleNode(node);
                }
            }
        }


        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is string) {
                string id = e.Node.Tag as string;

                HtmlDocument doc = webBrowser1.Document;
                HtmlElement ele = webBrowser1.Document.GetElementById(id);
                ele?.ScrollIntoView(true);
            }
        }

        #endregion


        #region MenuItem

        private void ItemRefresh_Click(object sender, EventArgs e)
        {
            RefreshShowNote();
        }

        private void ItemEdit_Click(object sender, EventArgs e)
        {
            EditNode();
        }

        private void ItemReLoad_Click(object sender, EventArgs e)
        {
            ReLoadTree();
        }

        private void ItemDelete_Click(object sender, EventArgs e)
        {
            DeleteNode();
        }

        private void ItemRename_Click(object sender, EventArgs e)
        {
            RenameSelectNote();
        }

        private void ItemExplorer_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null) {
                FileSystemInfo info = treeView1.SelectedNode.Tag as FileSystemInfo;
                Utility.OpenInExplorer(info.FullName);
            }
        }

        private void ItemAddNote_Click(object sender, EventArgs e)
        {
            AddNote();
        }

        private void ItemAddFold_Click(object sender, EventArgs e)
        {
            AddFold();
        }

        private void ItemExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void itemFontSize_Click(object sender, EventArgs e)
        {
            FormFontSize sizeForm = new FormFontSize();
            sizeForm.SetNumberValue(settings.FontSize);

            if (sizeForm.ShowDialog() == DialogResult.OK) {
                settings.FontSize = sizeForm.FontSize;
                RefreshShowNote();
            }
        }

        private void ItemAbout_Click(object sender, EventArgs e)
        {
            FormAbout about = new FormAbout();
            about.ShowDialog();
        }

        private void ItemHelp_Click(object sender, EventArgs e)
        {
        }

        private void ItemAddNote2_Click(object sender, EventArgs e)
        {
            AddNote(rootPath);
        }

        private void ItemAddFold2_Click(object sender, EventArgs e)
        {
            AddFold(rootPath);
        }

        #endregion

        #region webBrowser1

        private void webBrowser1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.F5) {
                RefreshShowNote();
                e.IsInputKey = true;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Document.Body.ScrollTop = refreshScrollTop;
        }

        #endregion

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {
            //e.ChangeType
            RefreshShowNote();
        }

        private void itemEditApp_Click(object sender, EventArgs e)
        {
            FormEditApp frmEdit = new FormEditApp();

            if (frmEdit.ShowDialog() == DialogResult.OK) {
                settings.Reload();
            }
        }

        private void itemIsCollapseBrother_Click(object sender, EventArgs e)
        {
            itemIsCollapseBrother.Checked = !itemIsCollapseBrother.Checked;

            settings.IsCollapseBrother = itemIsCollapseBrother.Checked;
        }

        private void itemOneClickMode_Click(object sender, EventArgs e)
        {
            itemOneClickMode.Checked = !itemOneClickMode.Checked;
            settings.OneClickMode = itemOneClickMode.Checked;
            setOneClickMode(settings.OneClickMode);
        }

        private void itemIsCollapseChilds_Click(object sender, EventArgs e)
        {
            // 暂时废弃
            //itemIsCollapseChilds.Checked = !itemIsCollapseChilds.Checked;
            //settings.IsCollapseChild = itemIsCollapseChilds.Checked;
        }

        private void itemAddWorkset_Click(object sender, EventArgs e)
        {
            FormAddWorkset form = new FormAddWorkset();

            if (form.ShowDialog() == DialogResult.OK) {

                var w = form.GetWorksetInfo();
                var path = w.Item1;
                var name = w.Item2;

                if (!Directory.Exists(path)) {
                    MessageBox.Show("工作集路径不存在", "LiteNote");

                } else if (name.Length == 0) {
                    MessageBox.Show("工作集名称不能为空", "LiteNote");

                } else {
                    settings.WorksetNames.Add(name);
                    settings.WorksetPaths.Add(path);
                    settings.Save();

                    AddRootNode(name, path);
                }

            }
        }

        private void contextItemWorksetRemove_Click(object sender, EventArgs e)
        {
            DeleteNode();
        }

        private void contextItemWorksetAddFold_Click(object sender, EventArgs e)
        {
            AddFold();
        }

        private void contextItemWorksetAddNote_Click(object sender, EventArgs e)
        {
            AddNote();
        }

        FormFind findForm;

        private void itemFind_Click(object sender, EventArgs e)
        {
            findForm.Show();
            findForm.Focus();
        }

        private void itemCharset_Click(object sender, EventArgs e)
        {

            FormCharset form = new FormCharset();
            form.setPath(nowNotePath);

            if (form.ShowDialog() == DialogResult.OK) {
                string s = form.getCharset();

                /*
                Console.WriteLine(s);
                Encoding en = Encoding.Default;

                try {
                    en = Encoding.GetEncoding(s);

                } catch (System.ArgumentException) { }

                if (en != null) {
                    Console.WriteLine("s:({0}) en:({1})", s, en);
                } else {
                    Console.WriteLine("null");
                }
                */
                AddCharset(nowNotePath, s);
                RefreshShowNote();
            }
        }

        private void item编辑_Click(object sender, EventArgs e)
        {
            //itemInsertImg.Enabled = false;
        }

        private void itemInsertImg_Click(object sender, EventArgs e)
        {
            if (!Utility.IsTextFile(nowNotePath)) {
                return;
            }

            if (openFileDialogInsertImg.ShowDialog() == DialogResult.OK) {

                string src = openFileDialogInsertImg.FileName;
                string dst = Utility.GetNoExistsImgName(Path.GetDirectoryName(nowNotePath), Path.GetFileName(src));

                File.Copy(src, dst);

                //StreamWriter sw = File.AppendText(nowNotePath);

                //string all = File.ReadAllText(nowNotePath);
                string[] ss = File.ReadAllLines(nowNotePath);

                string all = "";

                foreach (string s in ss) {
                    //File.WriteAllText()
                    all += s + "\n";
                }

                all += string.Format("![]({0})\n", Path.GetFileName(dst));
                File.WriteAllText(nowNotePath, all);
                //sw.Write();
                //sw.Close();

                RefreshNode(nowNoteNode);
            }
        }

    } // end NoteForm
} // end LiteNote
