namespace LiteNote
{
    partial class FormCharset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.rdbFix = new System.Windows.Forms.RadioButton();
            this.rdbOther = new System.Windows.Forms.RadioButton();
            this.cmbCharset = new System.Windows.Forms.ComboBox();
            this.txtCharset = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "文件路径";
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.Location = new System.Drawing.Point(145, 17);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(358, 28);
            this.txtPath.TabIndex = 1;
            // 
            // rdbFix
            // 
            this.rdbFix.AutoSize = true;
            this.rdbFix.Checked = true;
            this.rdbFix.Location = new System.Drawing.Point(12, 73);
            this.rdbFix.Name = "rdbFix";
            this.rdbFix.Size = new System.Drawing.Size(105, 22);
            this.rdbFix.TabIndex = 2;
            this.rdbFix.TabStop = true;
            this.rdbFix.Text = "设置编码";
            this.rdbFix.UseVisualStyleBackColor = true;
            // 
            // rdbOther
            // 
            this.rdbOther.AutoSize = true;
            this.rdbOther.Location = new System.Drawing.Point(12, 120);
            this.rdbOther.Name = "rdbOther";
            this.rdbOther.Size = new System.Drawing.Size(69, 22);
            this.rdbOther.TabIndex = 3;
            this.rdbOther.Text = "其它";
            this.rdbOther.UseVisualStyleBackColor = true;
            // 
            // cmbCharset
            // 
            this.cmbCharset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCharset.FormattingEnabled = true;
            this.cmbCharset.Items.AddRange(new object[] {
            "Default",
            "UTF-8",
            "GB2312"});
            this.cmbCharset.Location = new System.Drawing.Point(145, 69);
            this.cmbCharset.Name = "cmbCharset";
            this.cmbCharset.Size = new System.Drawing.Size(163, 26);
            this.cmbCharset.TabIndex = 5;
            // 
            // txtCharset
            // 
            this.txtCharset.Location = new System.Drawing.Point(145, 120);
            this.txtCharset.Name = "txtCharset";
            this.txtCharset.Size = new System.Drawing.Size(163, 28);
            this.txtCharset.TabIndex = 6;
            this.txtCharset.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(217, 186);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(140, 45);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(363, 186);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(140, 45);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FormCharset
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(532, 243);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtCharset);
            this.Controls.Add(this.cmbCharset);
            this.Controls.Add(this.rdbOther);
            this.Controls.Add(this.rdbFix);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCharset";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置字符集";
            this.Load += new System.EventHandler(this.FormCharset_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.RadioButton rdbFix;
        private System.Windows.Forms.RadioButton rdbOther;
        private System.Windows.Forms.ComboBox cmbCharset;
        private System.Windows.Forms.TextBox txtCharset;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}