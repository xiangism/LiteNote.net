using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteNote
{
    class SearchNote
    {
        // 是否搜索笔记的内容
        public bool IsSearchContent { get; set; }

        // 是否搜索笔记名
        public bool IsSearchNoteName { get; set; }

        // 是否搜索目录名
        public bool IsSearchFoldName { get; set; }

        // 是否大小写敏感
        public bool IsSearchAa { get; set; }

        List<string> searchPaths;
        // 设置搜索路径, 可以是目录或文件名
        public void setSearchPath(List<string> ps)
        {
            searchPaths = ps;
        }

        // 搜索到的所有结果
        // fullpath => detail
        Dictionary<string, string> searchResults = new Dictionary<string, string>();

        /*public Dictionary<string, string> SearchResults
        {
            get { return searchResults; }
            set { searchResults = value; }
        }*/

        // 搜索
        // @return (path, context) (路径,上下文)
        public Dictionary<string, string> search(string text)
        {
            searchResults.Clear();

            if (!IsSearchAa)
                text = text.ToLower();

            foreach (string path in searchPaths) {
                searchPath(path, text);
            }
            return searchResults;
        }

        void addOne(string path, string detail)
        {
            if (!searchResults.ContainsKey(path)) {
                searchResults[path] = detail;
            }
        }

        // @par path 不管是文件还是文件夹
        // @return (path, context) (路径,上下文)
        void searchPath(string path, string text)
        {
            if (File.Exists(path)) {
                // 是文件
                Tuple<bool, string> fr = searchNote(path, text);
                if (fr.Item1) {
                    addOne(path, fr.Item2);
                }

            } else if (Directory.Exists(path)) {
                // 是目录
                searchFold(path, text);
            }
        }

        // 搜索笔记文件
        // @return (isFind, context) (路径,上下文)
        Tuple<bool, string> searchNote(string path, string text)
        {
            Tuple<bool, string> r = new Tuple<bool, string>(false, "");

            if (!Utility.IsTextFile(path))
                return r;

            if (!IsSearchContent)
                return r;

            if (!File.Exists(path))
                return r;

            if (Utility.GetFileStyle(path) != 2) {
                return r;
            }

            // TODO:这里得从一个map中获取文件编码
            string[] lines = File.ReadAllLines(path);

            foreach (string line in lines) {
                if (IsSearchAa) {
                    // 大小写敏感
                    if (line.IndexOf(text) != -1) {
                        return new Tuple<bool, string>(true, line);
                    }

                } else {
                    // 不敏感
                    string s = line.ToLower();

                    if (s.IndexOf(text) != -1) {
                        return new Tuple<bool, string>(true, line);
                    }
                }
            }

            return r;
        }
        
        // 搜索目录
        void searchFold(string path, string text)
        {
            if (!Utility.IsNormalFold(path))
                return;

            DirectoryInfo info = new DirectoryInfo(path);

            // 所有文件
            FileInfo[] files = info.GetFiles();

            foreach (FileInfo one in files) {

                if (!Utility.IsTextFile(one.FullName))
                    continue;

                // 搜索文件的内容
                if (IsSearchContent) {
                    Tuple<bool, string> sr = searchNote(one.FullName, text);
                    if (sr.Item1) {
                        addOne(one.FullName, sr.Item2);
                    }
                }

                // 搜索文件名
                if (IsSearchNoteName) {
                    string src = one.FullName;
                    if (!IsSearchAa) {
                        src = src.ToLower();
                    }
                    if (Path.GetFileName(src).IndexOf(text) != -1) {
                        addOne(one.FullName, one.FullName);
                    }
                }
            }

            // 所有文件夹
            DirectoryInfo[] folds = info.GetDirectories();

            foreach (DirectoryInfo one in folds) {

                // 隐藏文件夹不搜索
                if ((one.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden) {
                    continue;
                }

                // 搜索文件夹名
                if (IsSearchFoldName) {

                    string src = one.FullName;
                    if (!IsSearchAa) {
                        src = src.ToLower();
                    }

                    if (Path.GetFileName(src).IndexOf(text) != -1) {
                        addOne(one.FullName, one.FullName);
                    }
                }

                searchFold(one.FullName, text);
            }
        }

    }
}
