namespace LiteNote
{
    partial class FormFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFind));
            this.btnFind = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbNote = new System.Windows.Forms.CheckBox();
            this.chbContent = new System.Windows.Forms.CheckBox();
            this.chbAa = new System.Windows.Forms.CheckBox();
            this.chbFold = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lsvResult = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFind
            // 
            this.btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFind.Location = new System.Drawing.Point(458, 32);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(140, 45);
            this.btnFind.TabIndex = 0;
            this.btnFind.Text = "查找";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(12, 42);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(416, 28);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "查找内容";
            // 
            // chbNote
            // 
            this.chbNote.AutoSize = true;
            this.chbNote.Checked = true;
            this.chbNote.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbNote.Location = new System.Drawing.Point(137, 104);
            this.chbNote.Name = "chbNote";
            this.chbNote.Size = new System.Drawing.Size(124, 22);
            this.chbNote.TabIndex = 3;
            this.chbNote.Text = "查找笔记名";
            this.chbNote.UseVisualStyleBackColor = true;
            this.chbNote.CheckedChanged += new System.EventHandler(this.chbNote_CheckedChanged);
            // 
            // chbContent
            // 
            this.chbContent.AutoSize = true;
            this.chbContent.Checked = true;
            this.chbContent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbContent.Location = new System.Drawing.Point(10, 104);
            this.chbContent.Name = "chbContent";
            this.chbContent.Size = new System.Drawing.Size(106, 22);
            this.chbContent.TabIndex = 4;
            this.chbContent.Text = "查找内容";
            this.chbContent.UseVisualStyleBackColor = true;
            this.chbContent.CheckedChanged += new System.EventHandler(this.chbContent_CheckedChanged);
            // 
            // chbAa
            // 
            this.chbAa.AutoSize = true;
            this.chbAa.Location = new System.Drawing.Point(10, 148);
            this.chbAa.Name = "chbAa";
            this.chbAa.Size = new System.Drawing.Size(124, 22);
            this.chbAa.TabIndex = 5;
            this.chbAa.Text = "大小写敏感";
            this.chbAa.UseVisualStyleBackColor = true;
            this.chbAa.CheckedChanged += new System.EventHandler(this.chbAa_CheckedChanged);
            // 
            // chbFold
            // 
            this.chbFold.AutoSize = true;
            this.chbFold.Checked = true;
            this.chbFold.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbFold.Location = new System.Drawing.Point(286, 104);
            this.chbFold.Name = "chbFold";
            this.chbFold.Size = new System.Drawing.Size(142, 22);
            this.chbFold.TabIndex = 7;
            this.chbFold.Text = "查找文件夹名";
            this.chbFold.UseVisualStyleBackColor = true;
            this.chbFold.CheckedChanged += new System.EventHandler(this.chbFold_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "fold.png");
            this.imageList1.Images.SetKeyName(1, "note.png");
            // 
            // lsvResult
            // 
            this.lsvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lsvResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lsvResult.FullRowSelect = true;
            this.lsvResult.HideSelection = false;
            this.lsvResult.LargeImageList = this.imageList1;
            this.lsvResult.Location = new System.Drawing.Point(10, 200);
            this.lsvResult.Name = "lsvResult";
            this.lsvResult.Size = new System.Drawing.Size(588, 501);
            this.lsvResult.SmallImageList = this.imageList1;
            this.lsvResult.TabIndex = 8;
            this.lsvResult.UseCompatibleStateImageBehavior = false;
            this.lsvResult.View = System.Windows.Forms.View.Details;
            this.lsvResult.SelectedIndexChanged += new System.EventHandler(this.lsvResult_SelectedIndexChanged);
            this.lsvResult.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lsvResult_MouseUp);
            this.lsvResult.Resize += new System.EventHandler(this.lsvResult_Resize);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "名称";
            this.columnHeader1.Width = 159;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "详情";
            this.columnHeader2.Width = 306;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(525, 116);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(1, 1);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormFind
            // 
            this.AcceptButton = this.btnFind;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(610, 713);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lsvResult);
            this.Controls.Add(this.chbFold);
            this.Controls.Add(this.chbAa);
            this.Controls.Add(this.chbContent);
            this.Controls.Add(this.chbNote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.btnFind);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFind";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "查找";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormFind_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFind_FormClosing);
            this.Load += new System.EventHandler(this.FormFind_Load);
            this.Enter += new System.EventHandler(this.FormFind_Enter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbNote;
        private System.Windows.Forms.CheckBox chbContent;
        private System.Windows.Forms.CheckBox chbAa;
        private System.Windows.Forms.CheckBox chbFold;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListView lsvResult;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnCancel;
    }
}